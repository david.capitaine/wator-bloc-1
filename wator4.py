import random
from time import sleep
import pylab

TPS_GEST_THON = 2
TPS_GEST_REQUIN = 6
ENERGIE = 3

def hauteur_grille(l):
    '''
    renvoie le nombre de lignes
    : param  grille : (list) grille de jeu
    :Exemple : >>> hauteur_grille(creer_grille(3, 2))
    2
    '''
    return len(l)   

def largeur_grille(l):
    '''
    renvoie le nombre de colonnes
    : param  grille : (list) grille de jeu de la vie
    : Exemple : >>> largeur_grille(creer_grille(3, 2))
    3
    '''
    return len(l[0])   
            
def creer_grille_vierge(nbLignes,nbColonnes):
    '''
    envoie une liste de listes
    correspondant à une grille du jeu de la vie aux dimensions souhaitées, ne
    contenant aucune cellule
    : param  nbLignes : (int) entier indiquant le nombre de lignes
    : param  nbColonnes : (int) entier indiquant le nombre de colonnes
    :Exemple : >>> creer_grille_vierge(3,2)
    [[[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    liste = []
    for l in range(0,nbLignes): # crée les lignes
        liste.append([])
        for _ in range(0,nbColonnes): # crée les lignes
            liste[l].append([0]*3) # crée [0,0,0]
    return liste

def afficher_grille(l):
    '''
    affiche la grille du jeu de vie _ cellule vide O Thons T Requin R.
    : param  l : (list) la grille de jeu de vie
    :Exemple : on considère que grille = [[[0,0,0], [1,2,0], [0,0,0]], [[2,2,1], [0,0,0], [0,0,0]], [[1,2,0], [2,2,2], [1,2,0]]]
    >>>> afficher_grille(grille)
    _ T _
    R _ _
    T R T
    >>> afficher_grille(creer_grille(3, 2))
    _ _ _
    _ _ _
    '''
    for ligne in range(0,len(l)):
        affichage = ""
        for colonne in range(0,len(l[0])):
            if l[ligne][colonne][0] == 1 :
                affichage = affichage + "T "
            elif l[ligne][colonne][0] == 2 :
                affichage = affichage + "R "
            elif l[ligne][colonne][0] == 0 :
                affichage = affichage + "_ "
        print(affichage)

            
def tirage_case(largeur, longueur):
    '''
    renvoie un tuple d'une case de la grille
    '''
    nb_alea = random.randrange(0, largeur * longueur - 1)
    colonne = nb_alea % largeur
    ligne = int(nb_alea / longueur)
    return ligne, colonne

def remplir_poissons(liste,num, pourcentage):
    '''
    envoie une liste de listes
    correspondant à une grille du jeu de la vie aux dimensions souhaitées,
    contenant des cellules suivant la probabilité
    : param  liste : (list) (list) grille de jeu
    : param  num : (int) entier indiquant un thon (1) ou un requin (2)
    : param  pourcentage : (int) entier indiquant le pourcentage de poissons à placer
    :Exemples : 
    >>> remplir_poissons(creer_grille_vierge(3,2),1,20)
    [[[1, 2, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    >>> remplir_poissons(creer_grille_vierge(3,2),1,40)
    [[[1, 2, 0], [0, 0, 0]], [[0, 0, 0], [1, 2, 0]], [[0, 0, 0], [0, 0, 0]]]
    >>> remplir_poissons(remplir_poissons(creer_grille_vierge(3,2),1,40),2,20)
    [[[1, 2, 0], [2, 4, 3]], [[1, 2, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    nb_poissons = int(hauteur_grille(liste) * largeur_grille(liste) * pourcentage/100)

    while (nb_poissons != 0):
        # on tire un nombre aléatoire entre 0 et 100 si la grille est de 10 * 10
        ligne, colonne = tirage_case(hauteur_grille(liste),largeur_grille(liste)) 
        #print('requin ', nb_requins, ' nb_alea ', nb_alea, ' ligne ', ligne, ' colonne ', colonne)
        if liste[ligne][colonne][0] == 0:
            if num == 1:
                liste[ligne][colonne] = [1, TPS_GEST_THON,0]
            elif num == 2:
                liste[ligne][colonne] = [2, TPS_GEST_REQUIN,ENERGIE]
            nb_poissons-=1
    return liste

def voisins_case(l,abscisse,ordonnee,num):
    '''
    ex  * abscisse = 5 et on cherche le voisin de gauche
        alors i = (4+25)%25 = 29%29 = 4 => inchangé !
        * abscisse = 0 et on cherche le voisin de gauche
        alors i = (-1+25)%25 = 24%25 = 24 => ça marche !!! Le voisin de 0, c est bien 24
        * abscisse = 24 et on cherche le voisin de droite
        alors i = (25+25)%25 = 50%25 = 0 => ça marche !!! Le voisin de 24, c est bien 0
    '''
    liste = []
    # test au nord
    i = (abscisse - 1 + largeur_grille(l)) % largeur_grille(l)
    if l[ordonnee][i][0] == num :
        liste.append([i,ordonnee])        
    # test au sud
    i = (abscisse + 1 + largeur_grille(l)) % largeur_grille(l)
    if l[ordonnee][i][0] == num :
        liste.append([i,ordonnee])
    # test à l'Est
    j = (ordonnee + 1 + hauteur_grille(l)) % hauteur_grille(l)
    if l[j][abscisse][0] == num :
        liste.append([abscisse,j])
    # test à l'Ouest
    j = (ordonnee - 1 + hauteur_grille(l)) % hauteur_grille(l)
    if l[j][abscisse][0] == num :
        liste.append([abscisse,j])
    return liste        

def calcul_deplacement(l):
    '''
    renvoie une tuple coord_ligne, coord_colonne en choisissant aléatoirement une possibilité dans l.
    : param  l : (list) la grille contenant les différents tuples des cases voisines possibles
    : CU : l ne doit pas être vide
    :Exemple :
    '''
    nb_alea = random.randrange(0, len(l))
    c_dest = l[nb_alea][0]
    l_dest = l[nb_alea][1]
    return l_dest, c_dest

def comportement_thon(l,lig, col):        
    if len(voisins_case(l,col,lig,0)) != 0: # s'il existe une case voisine vide
        l_dest, c_dest = calcul_deplacement(voisins_case(l,col,lig,0))
        # on déplace le thon
        # on copie les données de la case
        tps_gestation = l[lig][col][1]
        l[l_dest][c_dest] = [1, tps_gestation -1, 0]          
        # reproduction
        if l[l_dest][c_dest][1] == 0:
            l[lig][col] = [1,TPS_GEST_THON,0]
            l[l_dest][c_dest][1] = TPS_GEST_THON
        else:
            l[lig][col] = [0,0,0] # on efface la position précédente
    else:
        tps_gestation = l[lig][col][1] - 1
        if tps_gestation == 0:
            tps_gestation = TPS_GEST_THON
        l[lig][col] = [1,tps_gestation,0]
    return l

def comportement_requin(l,lig,col):
    # ---------DEPLACEMENT ----------
    l[lig][col][2] = l[lig][col][2] - 1
    #if l_dest == 0 and c_dest == 0: # s'il n'existe aucune case voisine avec un thon
    if len(voisins_case(l,col,lig,1)) != 0: # si on peut se déplacer sur une case avec un thon
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,1))
        tps_gest = l[lig][col][1]
        l[l_dest][c_dest] = [2,tps_gest-1,ENERGIE]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]
    elif len(voisins_case(l,col,lig,0)) != 0: # sinon si on peut se déplacer sur une case vide
        l_dest,c_dest = calcul_deplacement(voisins_case(l,col,lig,0)) # on choisit une case vide
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[l_dest][c_dest] = [2,tps_gest-1,energie]
        # suppression dans la case d'origine
        l[lig][col] = [0,0,0]     
    else :
        l_dest,c_dest = lig,col
        tps_gest = l[lig][col][1]
        energie = l[lig][col][2]
        l[lig][col] = [2,tps_gest-1,energie]
    # ---------ENERGIE ----------    
    if l[l_dest][c_dest][2] == 0: # si le niveau d'énergie tombe à 0
        l[l_dest][c_dest] = [0,0,0] # le requin meurt
    else:
        # ---------REPRODUCTION ----------
        if l[l_dest][c_dest][1] == 0:
            # reproduction seulement si il y a eu déplacement
            if(l_dest,c_dest) != (lig,col): 
                l[lig][col] = [2,TPS_GEST_REQUIN,ENERGIE]
                l[l_dest][c_dest][1] = TPS_GEST_REQUIN
            else: # réinitialisatiojn gestation
                l[l_dest][c_dest][1] = TPS_GEST_REQUIN
    return l

def generation_suivante(l):
    '''
    calcule la grille de la génération suivante et la retourne
    : param  l : (list) la grille de jeu
    :Exemple :
    [[[1, 2, 0], [2, 4, 3]], [[1, 2, 0], [0, 0, 0]], [[0, 0, 0], [0, 0, 0]]]
    '''
    # tirage d'une case aléatoirement dans la mer
    lig,col = tirage_case(largeur_grille(l), hauteur_grille(l))
    # si la case est un thon
    if l[lig][col][0] == 1:
        #print(l_dest,c_dest)
        return comportement_thon(l,lig,col)
    elif l[lig][col][0] == 2:
        #print('requin',' - ',lig, ' - ', col)
        return comportement_requin(l,lig,col)
    else :
        return l
        
def evolution_n_generations(l,n):
    '''
    affiche l'évolution de la grille au fil de n générations
    : param  l : (list) la grille de jeu de vie
    : param  n : (int) nombre de générations
    : param  temps_gestation_thons : (int) temps de gestation des thons
    : param  energie_requins : (int) niveau d'énergie des requins
    : param  temps_gestation_requins : (int) temps de gestation des requins
    :Exemple : 
    '''
    data_X = []
    data_Y_requins = []
    data_Y_thons = []

    for etape in range(0,n):
        if etape % 625:
            afficher_grille(l)
            print()
            # comptage du nombre de poissons
            nbr_requins = 0
            nbr_thons = 0
            for ligne in range(0,len(l)):
                for colonne in range(0,len(l[ligne])):
                    if l[ligne][colonne][0] == 1:
                        nbr_thons += 1
                    elif l[ligne][colonne][0] == 2:
                        nbr_requins += 1
            data_X.append(etape)
            data_Y_requins.append(nbr_requins)
            data_Y_thons.append(nbr_thons)     
            sleep(0.1)
        l = generation_suivante(l)
    pylab.plot(data_X, data_Y_thons)
    pylab.plot(data_X, data_Y_requins)
    pylab.title('évolution des requins et des thons')
    pylab.show()


def main_wator(nb_etapes) :
    grille = remplir_poissons(remplir_poissons(creer_grille_vierge(25,25),1, 30),2,10)
    evolution_n_generations(grille,nb_etapes)
    
if __name__ == '__main__':
    main_wator(125000)
